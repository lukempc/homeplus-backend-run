ALTER TABLE "user_info" DROP COLUMN  IF EXISTS "token";

DROP TABLE IF EXISTS "user_token";
CREATE TABLE "user_token" (
    "token_id" SERIAL PRIMARY KEY,
--    "email" INT REFERENCES "user_info" ("email"),
    "email" VARCHAR(255) UNIQUE NOT NULL,
    "token" VARCHAR(255),
    "create_time" TIMESTAMP WITH TIME ZONE,
    "expire_time" TIMESTAMP WITH TIME ZONE
);