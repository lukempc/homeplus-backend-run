INSERT INTO time_sections ("time_section_id", "title", "description") VALUES
(1, 'Morning', 'Before 10:00'),
(2, 'Midday', '10:00-13:00'),
(3, 'Afternoon', '13:00-17:00'),
(4, 'Evening', 'After 18:00');

INSERT INTO user_info ("name", "email", "password", "created_time", "updated_time") VALUES
('Mary', 'mary@mail.com', '12345', '2022-02-17 12:30:30', '2022-02-17 12:30:30'),
('John', 'john@mail.com', '54321', '2022-02-18 12:30:30', '2022-02-18 14:50:35'),
('Lucy', 'lucy@mail.com', '12345678', '2022-02-19 12:30:30', '2022-02-19 12:30:30'),
('Kevin', 'kevin@mail.com', '543210', '2022-02-21 12:30:30', '2022-02-21 14:50:35'),
('Wes', 'wes@mail.com', '543210', '2022-02-23 12:30:30', '2022-02-23 14:50:35');