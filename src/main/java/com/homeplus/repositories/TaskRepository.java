package com.homeplus.repositories;

import com.homeplus.models.TaskEntity;
import com.homeplus.models.TaskerEntity;
import com.homeplus.models.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

@Repository
@EnableJpaRepositories
public interface TaskRepository extends JpaRepository<TaskEntity, Long> {

    @Query("select t from TaskEntity as t where t.id = :id")
    Optional<TaskEntity> findById(@Param("id") Long id);

    @Query("select t from TaskEntity as t where t.title like %:keyword%")
    List<TaskEntity> findByKeyword(@Param("keyword") String keyword);

    @Query("select t from TaskEntity as t where (t.userEntity = :userEntity)")
    List<TaskEntity> findByUserId(@Param("userEntity") UserEntity userEntity);

    @Query("select t from TaskEntity as t where (t.taskerEntity = :taskerEntity)")
    List<TaskEntity> findByTaskerId(@Param("taskerEntity") TaskerEntity taskerEntity);

    @Query("select t from TaskEntity as t " +
            "where (t.userEntity = :userEntity) and " +
            "(t.date >= :currentDate) and " +
            "(t.date <= :upComingDay) " +
            "order by t.date")
    List<TaskEntity> findUpComingTasksByUid(@Param("userEntity") UserEntity userEntity, @Param("currentDate") OffsetDateTime currentDate, @Param("upComingDay") OffsetDateTime upComingDay);

    @Query("select t from TaskEntity as t " +
            "where (t.taskerEntity = :taskerEntity) and " +
            "(t.date >= :currentDate) and " +
            "(t.date <= :upComingDay) " +
            "order by t.date")
    List<TaskEntity> findUpComingTasksByTid(@Param("taskerEntity") TaskerEntity taskerEntity, @Param("currentDate") OffsetDateTime currentDate, @Param("upComingDay") OffsetDateTime upComingDay);

    @Query("select t from TaskEntity as t " +
            "where (t.taskerEntity = :taskerEntity)")
    List<TaskEntity> getTasksByTasker(TaskerEntity taskerEntity);
}
