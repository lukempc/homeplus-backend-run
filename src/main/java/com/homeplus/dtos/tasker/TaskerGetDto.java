package com.homeplus.dtos.tasker;

import com.homeplus.models.UserEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
public class TaskerGetDto {
    private Long id;

    private UserEntity userEntity;

    private String title;

    private String skills_description;

    private String certifications;

    private String introduction;

    private String category;

    private String bank_bsb;

    private String bank_account;

    private OffsetDateTime created_time;

    private OffsetDateTime updated_time;
}
