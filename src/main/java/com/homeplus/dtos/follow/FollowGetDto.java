package com.homeplus.dtos.follow;

import com.homeplus.models.TaskEntity;
import com.homeplus.models.UserEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
public class FollowGetDto {

    private Long id;

    private UserEntity userEntity;

    private TaskEntity taskEntity;

    private Boolean following;

    private OffsetDateTime created_time;

    private OffsetDateTime updated_time;
}
