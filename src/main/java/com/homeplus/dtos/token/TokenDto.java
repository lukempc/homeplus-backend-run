package com.homeplus.dtos.token;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TokenDto {
    private Long id;

    private String email;

    private String token;

    private OffsetDateTime createTime;

    private OffsetDateTime expireTime;
}
