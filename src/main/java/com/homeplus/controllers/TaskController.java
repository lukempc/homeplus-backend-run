package com.homeplus.controllers;

import com.homeplus.dtos.task.TaskGetDto;
import com.homeplus.dtos.task.TaskPostDto;
import com.homeplus.dtos.task.TaskPutDto;
import com.homeplus.services.TaskService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@Validated
public class TaskController {

    private final TaskService taskService;

    @GetMapping("tasks")
    public ResponseEntity<List<TaskGetDto>> find(){
        return ResponseEntity.ok(taskService.getAllTasks());
    }

    @GetMapping("task")
    public ResponseEntity<TaskGetDto> find(@RequestParam Long id){
        return ResponseEntity.ok(taskService.getTaskById(id));
    }

    @GetMapping("/task-ucoming")
    public ResponseEntity<List<TaskGetDto>> getUpComingTasksByUid(@RequestParam Long id) {
        return ResponseEntity.ok(taskService.getUpComingTasksByUid(id));
    }

    @GetMapping("/task-tcoming")
    public ResponseEntity<List<TaskGetDto>> getUpComingTasksByTid(@RequestParam Long id) {
        return ResponseEntity.ok(taskService.getUpComingTasksByTid(id));
    }

    @GetMapping("/task-ukwd")
    public ResponseEntity<List<TaskGetDto>> getTasksByUidAndKeyword(@RequestParam Long id, @RequestParam String keyword) {
        return ResponseEntity.ok(taskService.getTasksByUidAndKeyword(id, keyword));
    }

    @GetMapping("/task-tkwd")
    public ResponseEntity<List<TaskGetDto>> getTasksByTidAndKeyword(@RequestParam Long id, @RequestParam String keyword) {
        return ResponseEntity.ok(taskService.getTasksByTidAndKeyword(id, keyword));
    }

    @GetMapping("tasks-nearby")
    public ResponseEntity<List<TaskGetDto>> findTasksNearby(@RequestParam String address, @RequestParam Integer meters) {
        return ResponseEntity.ok(taskService.getNearbyTasks(address, meters, taskService.getAllTasks()));
    }

    @GetMapping("search-tasks")
    public ResponseEntity<List<TaskGetDto>> searchTasksByConditions(
            @RequestParam String address,
            @RequestParam Integer meters,
            @RequestParam String keyword){
        return ResponseEntity.ok(taskService.searchTasksByConditions(address, meters, keyword));
    }

    @PostMapping("post-task")
    public ResponseEntity<String> createTask(@Valid  @RequestBody TaskPostDto taskPostDto) {
        taskService.createTask(taskPostDto);
        return ResponseEntity.ok("success");
    }

    @DeleteMapping("delete-task")
    public ResponseEntity<String> deleteTask(@RequestParam Long id){
        taskService.deleteTask(id);
        return ResponseEntity.ok("task has been deleted");
    }

    @PutMapping("update-task")
    public ResponseEntity<String> updateTask(@Valid  @RequestBody TaskPutDto taskPutDto){
        taskService.updateTask(taskPutDto);
        return ResponseEntity.ok("success");
    }

    @PutMapping("cancel-task")
    public ResponseEntity<String> cancelTask(@RequestParam Long id){
        taskService.cancelTask(id);
        return ResponseEntity.ok("success");
    }
}
