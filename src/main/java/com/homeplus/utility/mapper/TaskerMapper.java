package com.homeplus.utility.mapper;

import com.homeplus.dtos.tasker.TaskerGetDto;
import com.homeplus.dtos.tasker.TaskerPostDto;
import com.homeplus.dtos.tasker.TaskerPutDto;
import com.homeplus.models.TaskerEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TaskerMapper {
    TaskerEntity postDtoToEntity(TaskerPostDto taskPostDto);

    TaskerEntity putDtoToEntity(TaskerPutDto taskerPutDto);

    TaskerGetDto fromEntity(TaskerEntity taskerEntity);

}
