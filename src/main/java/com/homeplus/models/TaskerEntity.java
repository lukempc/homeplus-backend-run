package com.homeplus.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Getter
@Setter
@Entity
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "tasker_info")
public class TaskerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tasker_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private UserEntity userEntity;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "skills_description")
    private String skills_description;

    @Column(name = "certifications")
    private String certifications;

    @Column(name = "category", nullable = false)
    private String category;

    @Column(name = "bank_bsb", nullable = false)
    private String bank_bsb;

    @Column(name = "bank_account", nullable = false)
    private String bank_account;

    @Column(name = "created_time", nullable = false)
    private OffsetDateTime created_time;

    @Column(name = "updated_time", nullable = false)
    private OffsetDateTime updated_time;
}
